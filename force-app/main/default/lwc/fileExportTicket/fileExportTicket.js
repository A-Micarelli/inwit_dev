import { LightningElement, wire } from 'lwc';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

import CASE_OBJECT from '@salesforce/schema/Case';
import STATUS_FIELD from '@salesforce/schema/Case.Status';

import exportTickets from '@salesforce/apex/CsvTicketController.exportTickets';

export default class FileExportTicket extends LightningElement {

    allStatus = { label: 'All Status', value: ''};
    isLoaded = false;
    isButtonDisabled = true;
    startDate;
    endDate;
    caseStatus = '';
    showSpinner = false;
    caseStatusPicklist;

    @wire(getObjectInfo, { objectApiName: CASE_OBJECT })
    caseMetadata;

    @wire(getPicklistValues,
        {
            recordTypeId: '$caseMetadata.data.defaultRecordTypeId', 
            fieldApiName: STATUS_FIELD
        }
    )
    wirePickValues({ error, data }) {
        if (data) {
            
            this.caseStatusPicklist = JSON.parse(JSON.stringify(data));
            this.caseStatusPicklist.values.unshift(this.allStatus);
            console.log(this.caseStatusPicklist);
            this.error = undefined;
            this.isLoaded = true;
        } else if (error) {
            this.error = error;
            this.caseStatusPicklist = undefined;
        }
    }
    
    // get options() {
    //     return [
    //         { label: 'All Status', value: ''},
    //         { label: 'New', value: 'New'},
    //         { label: 'In Charge', value: 'In Charge'},
    //         { label: 'Open', value: 'Open'},
    //         { label: 'In Charge To Bonificatore', value: 'In Charge To Bonificatore'},
    //         { label: 'In Progress', value: 'In Progress'},
    //         { label: 'To Be Verified', value: 'To Be Verified'},
    //         { label: 'Verified', value: 'Verified'},
    //         { label: 'Rejected', value: 'Rejected'},
    //         { label: 'Suspended', value: 'Suspended'},
    //         { label: 'Extra planned closed', value: 'Extra planned closed'},
    //         { label: 'Closed', value: 'Closed'},
    //         { label: 'Non di competenza', value: 'Non di competenza'},
    //         { label: 'Inoltrato', value: 'Inoltrato'},
    //         { label: 'In Analisi', value: 'In Analisi'},
    //         { label: 'Fattibile', value: 'Fattibile'},
    //         { label: 'Approvato', value: 'Approvato'},
    //         { label: 'Pianificato', value: 'Pianificato'},
    //         { label: 'Annullato', value: 'Annullato'},
    //         { label: 'Non Fattibile', value: 'Non Fattibile'},
    //     ];
    // }

    constructor(){
        super();
    }

    downloadCSVFile() { 
        this.showSpinner = true;
        exportTickets({ initialDate: this.startDate, 
                        endDate: this.endDate, 
                        status: this.caseStatus})
        .then(result => {
            this.showSpinner = false;
            this.handleReset();
            this.showNotification('Il tuo file è pronto!', 'A breve sarà inviato al tuo indirizzo email.', 'success'); 
        })
        .catch(error => {
            this.showSpinner = false;
            console.log('error ', error);
            this.handleReset();
            if(error && error.body && error.body.message.includes('Warning:')){
                this.showNotification('', error.body.message.replace('Warning:', ''), 'info');
            } else {
                this.showNotification('Qualcosa è andato storto!', 'Contattare un amministratore del sistema.', 'error');
            }
        });
    }

    showNotification(title, message, variant){
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
    }

    handleChange(event){
        console.log('campo ', event.target.name);
        console.log('valore ', event.target.value);

        this[event.target.name] = event.target.value;

        if(this.startDate && this.endDate){
            this.isButtonDisabled = false;
        } else {
            this.isButtonDisabled = true;
        }
    }

    handleReset() {
        const fields = this.template.querySelectorAll('lightning-input');
        if (fields) {
            fields.forEach(field => {
                field.reset();
            });
        }
        
        this.caseStatus = '';
        this.startDate = null;
        this.endDate = null;
        this.isButtonDisabled = true;
    }

}