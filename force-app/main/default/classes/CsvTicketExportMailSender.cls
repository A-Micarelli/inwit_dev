public with sharing class CsvTicketExportMailSender {

    private User user;
    private MailFactory mailFactory;
    public static final String EMAIL_TEMPLATE_NAME = 'Ticket Export Template';

    public CsvTicketExportMailSender(User user) {
        this.user = user;
        this.mailFactory = new MailFactory(EMAIL_TEMPLATE_NAME, new String[]{user.Email});
    }

    public void sendEmailWithAttachment(String filename, Blob bodyFile, String contentType) {
        try {
            this.mailFactory.getEmailTemplate();
            this.compileMail();
            this.mailFactory.setEmailParameters();
            this.mailFactory.insertAttachmentToEmail(filename, bodyFile, contentType);
            this.mailFactory.send();
        } catch (Exception e) {
            throw e;
        }
    }

    public void compileMail() {
        Map<String, String> placeholderMap = new Map<String, String>();
        placeholderMap.put('{Name}', this.user.FirstName);
        this.mailFactory.compileTemplate(placeholderMap);
    }

}