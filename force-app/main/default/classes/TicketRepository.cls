public without sharing class TicketRepository {
    
    public static List<Case> getCaseByRangeDateAndStatus(Date initialDate, Date endDate, String status) {
        List<Case> cases = new List<Case>();
        try {
            // cases = [
            //     SELECT Id, Name 
            //         FROM Case
            //             WHERE CreatedDate >= :initialDate AND CreatedDate <= :endDate AND Status = :status
            // ];

            String queryString = 'SELECT Id, Subject, CreatedDate, Status FROM Case ';
            String whereCondition = ' WHERE CreatedDate >= :initialDate AND CreatedDate <= :endDate ';

            if(String.isNotBlank(status)) {
                whereCondition += ' AND Status = :status ';
            }

            cases = (List<Case>) Database.query(queryString + whereCondition);

            return cases;
        } catch (DmlException e) {
            System.debug(LoggingLevel.ERROR, e.getMessage());
            throw e;
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, ex.getMessage());
            throw ex;
        }
    }

}