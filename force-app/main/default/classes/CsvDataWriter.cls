public with sharing class CsvDataWriter {

    private String columnDelimeter;

    public CsvDataWriter(String columnDelimeter) {
        this.columnDelimeter = columnDelimeter;
    }

    public Blob write(List<Map<String, String>> rows) {
        String csv = '';
        if(rows != null && rows.size() > 0) {
            for (Map<String, String> row : rows) {
                if(row != null && !row.isEmpty()) {
                    if(rows.indexOf(row) == 0) {
                        for(String header : row.keySet()) {
                            csv += header + this.columnDelimeter;
                        }
                        csv = csv.substring(0, csv.length() - 1) + '\n';
                    }
                    for (String value : row.values()) {
                        csv += value + this.columnDelimeter;
                    }
                    csv = csv.substring(0, csv.length() - 1) + '\n';
                }
            }
        }
        return Blob.valueOf(csv);
    }

}