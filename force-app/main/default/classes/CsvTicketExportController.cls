public with sharing class CsvTicketExportController {

    private CsvDataWriter csvWriter;

    public CsvTicketExportController(CsvDataWriter csvWriter) {
        this.csvWriter = csvWriter;
    }

    public Blob export(Date initialDate, Date endDate, String status) {
        List<Case> dataToExport = TicketRepository.getCaseByRangeDateAndStatus(initialDate, endDate, status);
        System.debug(dataToExport);
        Blob fileBody = this.csvWriter.write(this.serialize(dataToExport));
        System.debug(fileBody);
        return fileBody;
    }

    public List<Map<String, String>> serialize(List<Case> cases) {
        List<Map<String, String>> serializedData = new List<Map<String, String>>();
        for(Case caseToSerialize : cases) {
            Map<String, String> row = new Map<String, String>();
            row.put('Identificativo', caseToSerialize.Id);
            row.put('Subject', caseToSerialize.Subject);
            row.put('Data creazione', (caseToSerialize.CreatedDate != null) ? String.valueOf(caseToSerialize.CreatedDate) : '');
            row.put('Stato', caseToSerialize.Status);
            serializedData.add(row);
        }
        return serializedData;
    }

}