public without sharing class CsvMailFactory {

    private String emailTemplateName;

    private String emailBody;

    private String subject;

    private String[] toAddresses;

    private String fromAddress;

    private Messaging.SingleEmailMessage email;

    public CsvMailFactory(String templateName, String[] toAddresses) {
        this.emailTemplateName = templateName;
        this.toAddresses = toAddresses;
        this.email = new Messaging.SingleEmailMessage();
    }

    public void getEmailTemplate() {
        EmailTemplate emailTemplate = null;
        try {
            emailTemplate = [
                SELECT Id, Name, Subject, HtmlValue
                    FROM EmailTemplate
                        WHERE Name = :this.emailTemplateName LIMIT 1
            ];
            
            this.subject = emailTemplate.Subject;
            this.emailBody = emailTemplate.HtmlValue;

        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getMessage());
            throw e;
        }
    }

    public void setEmailParameters() {
        if(String.isNotBlank(this.subject)) {
            this.email.setSubject(this.subject);
        }

        if(String.isNotBlank(this.emailBody)) {
            this.email.setHtmlBody(this.emailBody);
        }

        if(this.toAddresses != null && this.toAddresses.size() > 0) {
            this.email.setToAddresses(this.toAddresses);
        }

        if(String.isNotBlank(this.fromAddress)) {
            this.email.setOrgWideEmailAddressId(this.fromAddress);
        }
    }

    public void insertAttachmentToEmail(String filename, Blob body, String contentType) {
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();

        efa.setFileName(filename);
        efa.setBody(body);
        efa.setContentType(contentType);

        this.email.setFileAttachments(new Messaging.EmailFileAttachment[]{efa});
    }

    public void send() {
        try {
            Messaging.SingleEmailMessage[] emailMessages = new List<Messaging.SingleEmailMessage> {this.email};
            Messaging.SendEmailResult[] emailSender = Messaging.sendEmail(emailMessages);
            
            if(emailSender[0].success) {
                System.debug(LoggingLevel.INFO, 'Mail inviata con successo');
            } else {
                System.debug(LoggingLevel.ERROR, 'Invio email in errore');
            }

        } catch (Exception e) {
            throw e;
        }
    }

    public void compileTemplate(Map<String, String> placeholderMap) {
        String data = '';
        if(placeholderMap != null && !placeholderMap.isEmpty()) {
            for (String key : placeholderMap.keySet()) {
                data = placeholderMap.get(key);
                this.emailBody = this.emailBody.replace(key, data);
            }
        }
    }
}