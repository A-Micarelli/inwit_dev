public without sharing class CsvTicketController {
    
    @AuraEnabled
    public static void exportTickets(Date initialDate, Date endDate, String status){
        try {
            CsvTicketExportController csvExport = new CsvTicketExportController(new CsvDataWriter(','));
            Blob bodyFile = csvExport.export(initialDate, endDate, status);
            if(bodyFile.size() > 0) {
                sendEmail(bodyFile);
            } else {
                throw new AuraHandledException('Nessun record trovato');
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static void sendEmail(Blob mailAttachment) {
        try {
            User currentUser = [SELECT Email, FirstName FROM User WHERE Id = :UserInfo.getUserId()];
            CsvTicketExportMailSender sender = new CsvTicketExportMailSender(currentUser);
            sender.sendEmailWithAttachment('Export_Ticket_' + Datetime.now() + '.csv', mailAttachment, 'text/csv');
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}